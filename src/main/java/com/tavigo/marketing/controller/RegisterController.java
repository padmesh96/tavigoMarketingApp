package com.tavigo.marketing.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tavigo.marketing.entity.Register;
import com.tavigo.marketing.repository.RegistrationRepository;


@RestController
@RequestMapping("/api")
public class RegisterController {
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@PostMapping("/register")
	public ResponseEntity<String> createEmployee(@RequestBody Register driver) {
		Register driverDetailList = registrationRepository.save(driver);
		if(Objects.nonNull(driverDetailList)) {
			return new ResponseEntity<>("Success", HttpStatus.OK);	
		}
		return new ResponseEntity<>("Failed", HttpStatus.INTERNAL_SERVER_ERROR);	
	}
}
