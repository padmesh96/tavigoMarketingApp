package com.tavigo.marketing.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.tavigo.marketing.entity.Register;


public interface RegistrationRepository extends JpaRepository<Register, String> {

}