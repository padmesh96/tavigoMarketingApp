package com.tavigo.marketing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "register")
@Data
public class Register {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "regnum")
	private int regnum;

	@Column(name = "name")
	private String name;
	
	@Column(name = "dateofbirth")
	private String dateofbirth;

	@Column(name = "address")
	private String address;

	@Column(name = "vehiclenumber")
	private int vehiclenumber;

	@Column(name = "rcbooknum")
	private int rcbooknum;

	@Column(name = "isownvehicle")
	private boolean isownvehicle;
	
}
